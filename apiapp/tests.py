from django.test import TestCase,Client,LiveServerTestCase
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time


# Create your tests here.

class UnitTest(TestCase):
    def test_ada_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_ada_tabel(self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn ("<table", content)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Library', self.browser.title)

        inputbox1 = self.browser.find_element_by_id('search')
        time.sleep(2)
        inputbox1.send_keys('teknik')
        inputbox1.send_keys(Keys.ENTER)
        time.sleep(2)

